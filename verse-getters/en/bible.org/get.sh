#!/usr/bin/bash

verse="$(curl "https://labs.bible.org/api/?passage=random")"
verse="$(echo "$verse" | sed 's/<[^>]*>//g')"
echo "$verse"
