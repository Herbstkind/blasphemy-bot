#!/usr/bin/bash

scriptdir="$(dirname "$0")"
verse=''
while [ ${#verse} -lt 100 ]; do
    verse="$(shuf -n 1 "$scriptdir"/Martin_Luther_Uebersetzung_1912.txt)"
done
echo "$verse"
