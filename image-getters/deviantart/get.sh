#!/usr/bin/bash

keywords='furry Hentai Bondage gore leather jellyfish waifu maids'
tag=furry


domain='www.deviantart.com'
oauth="https://$domain/oauth2"
api="https://$domain/api/v1/oauth2"

scriptdir=$(dirname "$0")
client_id="$(cat "$scriptdir"/client_id)"
client_secret="$(cat "$scriptdir"/client_secret)"

function error {
    echo "$1" >/dev/stderr
}

# fetch and check token
token_response="$(curl "$oauth/token" -d grant_type=client_credentials -d client_id="$client_id" -d client_secret="$client_secret")"
token="$(echo "$token_response" | jq .access_token | sed 's/"//g')"
token_status_response="$(curl "$api/placebo" -d access_token="$token")"
token_status="$(echo "$token_status_response" | jq .status | sed 's/"//g')"
if [ "$token_status" != "success" ]; then
    error "Invalid token. client_id=$client_id client_secret=$client_secret token=$token"
    exit 1
fi

# get some sick stuff
#curl "$api/browse/newest" -d "access_token=$token" -d "mature_content=true" -d "q=$keywords"
deviations_response="$(curl "$api/browse/tags?tag=$tag&offset=$((RANDOM % 100))" \
    -d "access_token=$token" -d "mature_content=true" | tee "$scriptdir"/deviations.response)"
index=$((RANDOM % 10))
found='no'
while [ "$found" != 'yes' ]; do
    url="$(echo "$deviations_response" | jq ".results[$index].content.src" | sed 's/"//g')"
    if [ "$url" == "null" ]; then
        index=$((index - 1))
    else
        found='yes'
    fi
done

# return
echo "$url"
