# Blasphemy Bot
*There was given to him a mouth speaking arrogant words and blasphemies - Revelation 13:5*<br>

This bot posts a random furry image accompanied by a random bible verse.

![](./upright_men.png)
<br>(Artwork by [InkTiger](https://www.deviantart.com/inktigerart/art/Furries-Illustrated-4-Zeno-812575636))

On my server I let the bot run every Sunday at 6+6+6 o'clock, that's why it says so in the greeting text.
<br>Possible languages are german or english, just change the getter script paths in `./blasphemy`.

## Usage overview
Install dependencies: bash, coreutils, curl, jq, sed<br>
Place the webhook of the target channel in `./hook`.

By default, blasphemy bot uses e621 to get images.
If you want to use deviantart, you have to [register an application](https://www.deviantart.com/developers/authentication) first and place its ID into `image-getters/deviantart/client_id`, its secret into `image-getters/deviantart/client_secret`.

Then, just run `./blasphemy`, that's it.<br>
Possible arguments are (if both are supplied, they have to be in this order):
- `--dry-run`: Don't post anything, just print to stdout what would be posted
- `--no-greeting`: Leave out the greeting text. Use this if it's not Sunday 6+6+6 o'clock :)

## Step by step instructions
1) Prepare your environment. You need the bash shell to run this bot. On Linux you already have one, just open a terminal. On Windows download [Cygwin](https://www.cygwin.com/) and open their terminal.
2) Install dependencies: `coreutils` (usually already installed), `curl`, `jq`, `sed`. On Linux just use your package manager. On Windows (Cygwin) run the cygwin installer again and select the packages from the list.
3) Create a Discord webhook. See [Discords instruction](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks) under the heading *Making a Webhook*. A webhook is just an address attached to a channel. When you send text to that address it will appear in the channel.
4) Copy the webhook URL and paste it into a file in this directory called `hook`. (not `hook.txt` or something!)
5) From a bash shell (open a terminal, on Windows it's the Cygwin terminal) go to this directory and type `./blasphemy`, then press enter.
